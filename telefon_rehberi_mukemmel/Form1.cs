﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace telefon_rehberi_mukemmel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnKisiEkle_Click(object sender, EventArgs e)
        {
            KisiEkle();
            
        }

        private void KisiEkle()
        {
            Kisi K1 = new Kisi{ Isim = txtIsim.Text, Soyisim= txtSoyisim.Text, Tel_No= txtTel_No.Text, Email= txtEmail.Text };
            Database.KisiListesi.Add(K1);
            lstKisi.Items.Add(K1);
           
        }

        private void lstKisi_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox Secim = (ListBox)sender;
            Kisi SecilenKisi = (Kisi)Secim.SelectedItem;

            
            txtIsim.Text = SecilenKisi.Isim;
            txtSoyisim.Text = SecilenKisi.Soyisim;
            txtTel_No.Text = SecilenKisi.Tel_No;
            txtEmail.Text = SecilenKisi.Email;
        }

        private void btnKisiSil_Click(object sender, EventArgs e)
        {
            lstKisi.Items.Remove(lstKisi.SelectedItem);           
            
        }
    }
}
