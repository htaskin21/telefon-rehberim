﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telefon_rehberi_mukemmel
{
    public class Kisi
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string Soyisim { get; set; }
        public string Tel_No { get; set; }
        public string Email { get; set; }

        private static int IdSayac;

        static Kisi()
        {
            IdSayac = 1;
        }

        public Kisi()
        {
            IdSayac = Id;
            IdSayac++;
        }

        public override string ToString()
        {
            return String.Format($"{Isim } {Soyisim }");
        }
    }
}
